#!/usr/bin/env python

for i in range(1, 21): print(("" if i % 3 else "fizz")+("" if i % 5 else "buzz") or i)

# print("\n".join(["fizz"*(i%3==0) + "buzz"*(i%5==0) or str(i) for i in range(1,21)]))
