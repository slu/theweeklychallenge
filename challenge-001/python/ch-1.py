#!/usr/bin/env python

import re

string = "Perl Weekly Challenge"
(string, count) = re.subn("e", "E", string)

assert string == "PErl WEEkly ChallEngE", f"Replacement not as expected: {string}"

print(f"Count: {count}")
