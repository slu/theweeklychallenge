#!/usr/bin/env perl

use v5.36;
use Capture::Tiny ':all';
use List::MoreUtils qw(uniq);

sub solution_1 {
    say "1\n2\nfizz\n4\nbuzz\nfizz\n7\n8\nfizz\nbuzz\n11\nfizz\n13\n14\nfizzbuzz\n16\n17\nfizz\n19\nbuzz";
}

sub solution_2 {
    say (($_ % 3 ? "":"fizz").($_ % 5 ? "":"buzz") || $_) for 1..20;
}

sub solution_3 {
    say join "\n", map { ($_%3==0 && "fizz").($_%5==0 && "buzz") || $_ } 1..20;
}

my @solutions = (\&solution_1, \&solution_2, \&solution_3);

my @output = uniq map { my $o = capture_stdout { $solutions[$_]->() }; $o } 0..$#solutions;
die "Output mismatch" if @output > 1;
print $output[0];
