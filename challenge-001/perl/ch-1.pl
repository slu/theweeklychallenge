#!/usr/bin/env perl

# Solutions to task #1 of Weekly Challenge #1.
#
#     "Write a script to replace the character ‘e’ with ‘E’ in the
#     string ‘Perl Weekly Challenge’. Also print the number of times
#     the character ‘e’ is found in the string."
#
# See https://theweeklychallenge.org/blog/perl-weekly-challenge-001/
#

use v5.36;
use List::MoreUtils qw(uniq);


sub solution_0($string) {
    return ("PErl WEEkly ChallEngE", 5);
}


sub solution_1($string) {
    my $count = 0;
    for (my $i = 0; $i < length($string); $i++) {
        my $char = substr($string, $i, 1);
        if ($char eq 'e') {
            substr($string, $i, 1) = uc $char;
            $count++;
        }
    }
    return ($string, $count);
}


sub solution_2($string) {
    my $count = 0;

    while((my $position = index($string, "e")) >= 0) {
        substr($string, $position, 1) = "E";
        $count++;
    }

    return ($string, $count);
}


sub solution_3($string) {
    my $count = $string =~ tr/e/E/;
    return ($string, $count);
}


my @solutions = (\&solution_0, \&solution_1, \&solution_2, \&solution_3);

my $string = "Perl Weekly Challenge";
my @output = uniq map { join "\n", $solutions[$_]->($string) } 0..$#solutions;
die "Output mismatch" if @output > 1;
say $output[0];
