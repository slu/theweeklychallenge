#!/usr/bin/env perl

# Solutions to task #1 of Weekly Challenge #249.
#
# See https://theweeklychallenge.org/blog/perl-weekly-challenge-249/#TASK1
#

use v5.36;
use List::Util qw(all pairs);
use List::MoreUtils qw(uniq);
use Data::Dump qw(dd);


sub solution_1(@ints) {
    my @sorted = sort @ints;
    my @result = ();
    for(my $number = 0; $number < $#sorted; $number+=2) {
        return [] if $sorted[$number] ne $sorted[$number+1];
        push @result, [$sorted[$number], $sorted[$number+1]];
    }

    return \@result;
}


sub solution_2(@ints) {
    return map { [ $_->[0], $_->[1] ]} grep { $_->[0] eq $_->[1] } pairs sort @ints;
}


my @solutions = (\&solution_1, &solution_2);

for my $ints ([3, 2, 3, 2, 2, 2], [1, 2, 3, 4]) {
    say "Input: \@ints = (", join(", ", @$ints), ")";
    my @output = uniq map { $solutions[$_]->(@$ints) } 0..$#solutions;
    die "Output mismatch" if @output > 1;
    print "Output: ";
    if ($#{$output[0]} < 1) {
        say "()";
    } else {
        say join ", ", map { "(" . join(", ", @{$_}) . ")" } @{$output[0]};
    }
}
