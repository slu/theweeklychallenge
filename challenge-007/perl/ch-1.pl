#!/usr/bin/env perl

use v5.36;
use List::Util qw(sum);
use List::MoreUtils qw(uniq);

sub solution_0() {
    return (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 18, 20, 21, 24, 27, 30, 36, 40, 42, 45, 48, 50);
}


sub solution_1() {
    my @niven;
    for my $n (0..50) {
        my $sum = sum split "", $n;
        if ($sum > 0 && $n % $sum == 0) {
            push @niven, $n;
        }
    }
    return @niven;
}

sub solution_2() {
    return grep { $_ gt 0 && $_ % ( sum split "", $_ ) == 0 } (0..50);
}


sub is_niven($n) {
    return $n gt 0 && $n % ( sum split "", $n ) == 0;
}

sub solution_3() {
    return grep { is_niven($_) } (0..50);
}


my @solutions = (\&solution_0, \&solution_1, \&solution_2, \&solution_3);

my @output = uniq map { join "\n", $solutions[$_]->() } 0..$#solutions;
die "Output mismatch" if @output > 1;
say $output[0];
