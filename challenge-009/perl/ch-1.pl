#!/usr/bin/env perl

use v5.36;
use List::MoreUtils qw(uniq);

sub solution_1() {
    my $n = 1;

    while(1) {
        my @digits = split "", $n ** 2;
        my %unique = map { $_ => 1 } @digits;
        last if scalar %unique >= 5;
        $n++;
    }

    return $n;
}

sub solution_2() {
    my $n = 0; # this will be pre-incremented in loop, so we actually start from 1
    my $unique = 0;

    $unique = scalar keys %{{map { $_ => 1 } split "", ++$n ** 2}} while $unique < 5;

    return $n;
}


my @solutions = (\&solution_1, \&solution_2);

my @output = uniq map { join "\n", $solutions[$_]->() } 0..$#solutions;
die "Output mismatch" if @output > 1;
say $output[0];

