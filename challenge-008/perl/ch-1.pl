#!/usr/bin/env perl

use v5.36;
use List::MoreUtils qw(uniq);

sub solution_0() {
    return (6, 28, 496, 8128, 33550336);
}


my @solutions = (\&solution_0);#, \&solution_1);

my @output = uniq map { join "\n", $solutions[$_]->() } 0..$#solutions;
die "Output mismatch" if @output > 1;
say $output[0];
