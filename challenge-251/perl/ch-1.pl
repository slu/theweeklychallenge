#!/usr/bin/env perl

# Solutions to task #1 of Weekly Challenge #251.
#
# See https://theweeklychallenge.org/blog/perl-weekly-challenge-251/#TASK1
#

use v5.36;
use List::Util qw(zip sum);
use List::MoreUtils qw(uniq);


sub solution_1(@ints) {
    my $concatenation_value = 0;

    $concatenation_value += shift(@ints) . pop(@ints) for 0..(scalar @ints/2)-1;
    $concatenation_value += $ints[0] if scalar @ints;

    return $concatenation_value;
}


sub solution_2(@ints) {
    my $concatenation_value = 0;

    $concatenation_value += $ints[$_] . $ints[$#ints - $_] for 0..(scalar @ints/2)-1;
    $concatenation_value += $ints[scalar @ints/2] if scalar @ints % 2;

    return $concatenation_value;
}


sub solution_3(@ints) {
    return sum
           map { $_->[0] . ( $_->[1] || "" ) }
           zip [@ints[0..($#ints/2)]],
               [reverse @ints[(scalar @ints/2+.5)..$#ints]];
}


my @solutions = (\&solution_1, \&solution_2, \&solution_3);

for my $ints ([6,12,25,1], [10, 7, 31, 5, 2, 2], [1, 2, 10]) {
    say "Input: \@ints = (", join(", ", @$ints), ")";
    my @output = uniq map { $solutions[$_]->(@$ints) } 0..$#solutions;
    die "Output mismatch" if @output > 1;
    say "Output: ", $output[0];
}
