#!/usr/bin/env perl

use v5.36;
use List::Util qw(min max);
use List::MoreUtils qw(uniq);
use Algorithm::Loops qw(MapCar);

use Data::Dump qw(dd);

sub solution_1($matrix) {
    my @min = map { min @$_ } @$matrix;
    my @max = map { max @$_}  (MapCar {[@_]} @$matrix);

    my %in_min = ();
    $in_min{$_}++ for @min;

    return grep { $in_min{$_} } @max;
}

my @solutions = (\&solution_1);

sub print_matrix($matrix) {
    print "Input: \$matrix = [ ";
    for my $y (0..$#{$matrix}) {
        printf "%s[", $y > 0 ? " "x19 : "";
        for my $x (0..$#{$matrix->[$y]}) {
            printf "%*d%s", $x < 1 ? 2 : 3, $matrix->[$y][$x], $x < $#{$matrix->[$y]} ? "," : "";
        }
        printf "]%s", $y < $#{$matrix} ? ",\n" : "";
    }
    print " ];\n";
}

for my $matrix ([ [ 3,  7,  8],
                  [ 9, 11, 13],
                  [ 15, 16, 17] ],
                [ [ 1, 10,  4,  2],
                  [ 9,  3,  8,  7],
                  [15, 16, 17, 12] ],
                [ [7 ,8],
                  [1 ,2] ]) {

    print_matrix($matrix);
    say "Output: ", solution_1($matrix);
}

