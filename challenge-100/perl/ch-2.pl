#!/usr/bin/env perl

use v5.36;
use List::Util qw(max);
use List::MoreUtils qw(uniq);

sub calc_maximum_value1(@alphanumstr) {
    my $max = 0;
    foreach my $val (@alphanumstr) {
        if ($val =~ /^\d+$/) {
            $max = int($val) if $val > $max;
        } else {
            $max = length($val) if length($val) > $max;
        }
    }
    return $max;
}


sub calc_maximum_value2(@alphanumstr) {
    return max map { $_ =~ /^\d+$/ ? int($_) : length($_) } @alphanumstr;
}


my @solutions = (\&calc_maximum_value1, \&calc_maximum_value2);

for my $alphanumstr (["perl", "2", "000", "python", "r4ku"],
                     ["001", "1", "000", "0001"]) {
    say "Input: \@alphanumstr = (", join(", ", @$alphanumstr), ")";
    my @output = uniq map { $solutions[$_]->(@$alphanumstr) } 0..$#solutions;
    die "Output mismatch" if @output > 1;
    say "Output: ", $output[0];
}
