#!/usr/bin/env perl

# Solutions to task #1 of Weekly Challenge #100.
#
# See https://theweeklychallenge.org/blog/perl-weekly-challenge-100/#TASK1
#

use v5.36;
use List::MoreUtils qw(uniq);
use Time::Piece;


sub solution_1($time) {
    state @fmt = ("%H:%M", "%I:%M %P");
    my $idx = $time =~ /[ap]m/ || 0;
    return Time::Piece->strptime($time, $fmt[$idx])->strftime($fmt[1-$idx]);
}



sub solution_2($time) {
    if ($time =~ /(\d{2}):(\d{2}) *([ap]m)?/) {
        my ($hour, $min, $period) = ($1, $2, $3);
        if (defined $period) {
            $hour += 12 if $period eq "pm";
            $period = "";
        } elsif ($hour > 11) {
            $hour -= 12;
            $period = " pm";
        } else {
            $period = " am";
        }
        return sprintf "%02d:%02d%s", $hour, $min, $period;
    } else {
        die "Unrecognized time format: $time";
    }
}


my @solutions = (\&solution_1, \&solution_2);

for my $time ("05:15 pm", "05:15pm", "19:15", "03:33 am", "04:44") {
    say "Input: \$time = \"$time\"";
    my @output = uniq map { $solutions[$_]->($time) } 0..$#solutions;
    die "Output mismatch" if @output > 1;
    say "Output: \"$output[0]\"";
}
