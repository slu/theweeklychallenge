#!/usr/bin/env python

import fileinput
import re

from operator import xor

for line in fileinput.input():
    valid_line = re.match("^(?P<col>[a-h])(?P<row>[1-8])$", line)
    if not valid_line:
        print(f"Bad input: {line}")
        continue
    print(str(xor((ord(valid_line.group("col")) - ord('a') + 1) % 2 == 0,
                  int(valid_line.group("row")) % 2 == 0)).lower())
