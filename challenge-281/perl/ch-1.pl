#!/usr/bin/env perl

use v5.40;

while (<>) {
    if (!/^(?<col>[a-h])(?<row>[1-8])$/) { say "Bad input: $_"; next }
    say((ord($+{col}) - ord('a') + 1) % 2 == 0 ^^ $+{row} % 2 == 0 ? "true" : "false");
}
