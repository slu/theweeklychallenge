#!/usr/bin/env python

"""Solutions to task #1 of Weekly Challenge #250.

See https://theweeklychallenge.org/blog/perl-weekly-challenge-250/#TASK1

Given an array of integers, ints, find the smallest index where the
index modulus 10 is equal to the corresponding value, e.g., we want
the index where the following condition is true: idx % 10 == ints[idx]

"""

def find_smallest_idx_where_idx_mod_10_is_equal_to_element1(ints):
    """A basic iterative approach. Looping over the range of indexes
    and returning when condtion is true. If we do not return in the
    loop, then the default is to return -1.    

    """
    # pylint: disable=consider-using-enumerate
    for idx in range(len(ints)):
        if idx % 10 == ints[idx]:
            return idx
    return -1


def find_smallest_idx_where_idx_mod_10_is_equal_to_element2(ints):
    for idx, val in enumerate(ints):
        if idx % 10 == val:
            return idx
    return -1


def find_smallest_idx_where_idx_mod_10_is_equal_to_element3(ints):
    if any((first := idx) % 10 == ints[idx] for idx in range(len(ints))):
        return first
    return -1


def find_smallest_idx_where_idx_mod_10_is_equal_to_element4(ints):
    try:
        return next(idx for idx, val in enumerate(ints) if idx % 10 == val)
    except StopIteration:
        return -1


for ints in [[0,1,2], [4, 3, 2, 1], [1, 2, 3, 4, 5, 6, 7, 8, 9, 0]]:
    print(f"ints = {ints}")
    print(find_smallest_idx_where_idx_mod_10_is_equal_to_element1(ints))
    print(find_smallest_idx_where_idx_mod_10_is_equal_to_element2(ints))
    print(find_smallest_idx_where_idx_mod_10_is_equal_to_element3(ints))
    print(find_smallest_idx_where_idx_mod_10_is_equal_to_element4(ints))
