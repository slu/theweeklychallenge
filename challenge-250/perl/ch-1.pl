#!/usr/bin/env perl

# Solutions to task #1 of Weekly Challenge #250.
#
# See https://theweeklychallenge.org/blog/perl-weekly-challenge-250/#TASK1
#
# Given an array of integers, @ints, find the smallest index where
# index modulus 10 is equal to the corresponding value, e.g., we want
# the index where the following condition is true:
#
#   $idx % 10 == @ints[$idx]
#

use v5.36;
use List::Util qw(first);
use List::MoreUtils qw(first_index uniq);


sub solution_1(@ints) {
    for my $idx (0..$#ints) {
        return $idx if $idx % 10 == $ints[$idx];
    }
    return -1;
}


sub solution_2(@ints) {
    while (my ($idx, $int) = each @ints) {
        return $idx if $idx % 10 == $int;
    }
    return -1;
}


sub solution_3(@ints) {
    my $first = first { $_ % 10 == $ints[$_] } 0..$#ints;
    return defined $first ? $first : -1;
}


sub solution_4(@ints) {
    return first_index { $_ % 10 == $ints[$_] } 0..$#ints;
}


my @solutions = (\&solution_1, \&solution_2, \&solution_3, \&solution_4);

for my $ints ([0,1,2], [4, 3, 2, 1], [1, 2, 3, 4, 5, 6, 7, 8, 9, 0]) {
    say "Input: \@ints = (", join(", ", @$ints), ")";
    my @output = uniq map { $solutions[$_]->(@$ints) } 0..$#solutions;
    die "Output mismatch" if @output > 1;
    say "Output: ", $output[0];
}
