#!/usr/bin/env perl

use v5.36;
use Scalar::Util qw(looks_like_number);

sub solution_1($positive_number) {
    $positive_number =~ s/^0+// if looks_like_number($positive_number);
    return substr($positive_number,0,1) eq '.' ? "0$positive_number" : $positive_number;
}

sub solution_2($positive_number) {
    return looks_like_number($positive_number) ? 0 + $positive_number : $positive_number;
}

say solution_1($_) for qw(1 02 030 004 50 000006 1.0 02.0 0.30 0.04 .50 000.006 1b 0x3 number 0-width);

say solution_2($_) for qw(1 02 030 004 50 000006 1.0 02.0 0.30 0.04 .50 000.006 1b 0x3 number 0-width);
