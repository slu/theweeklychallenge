#!/usr/bin/env perl

use v5.36;
use List::Util qw(reduce);

my $offset = ord("A")-10;

sub hex_to_dec($hex) {
    my @digits = map { /[A-F]/ ? ord($_) - $offset : $_ } split "", $hex;
    my $dec = 0;

    for my $idx (0..$#digits) {
        $dec += $digits[$idx];
        $dec *= 16 if $idx < $#digits;
    }

    return $dec;
}

sub base35_to_dec($hex) {
    my @digits = map { /[A-Y]/ ? ord($_) - $offset : $_ } split "", $hex;
    my $dec = 0;

    for my $idx (0..$#digits) {
        $dec += $digits[$idx];
        $dec *= 35 if $idx < $#digits;
    }

    return $dec;
}

sub to_dec($number, $base) {
    return reduce { $a * $base + $b } map { "A" le $_ le chr($offset+$base-1) ? ord($_) - $offset : $_ } split "", $number;
}

my $hex = "12AC";
say "\$$hex = ", hex_to_dec($hex), " / ", to_dec($hex, 16);
my $base35 = "ABCDEFG";
say "\$$base35 = ", base35_to_dec($base35), " / ", to_dec($base35, 35);
